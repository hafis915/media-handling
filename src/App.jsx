import logo from './logo.svg';
import './App.css';
import ReactPlayer from 'react-player'
import { CSVLink } from 'react-csv'
import { Document, Page , pdfjs} from 'react-pdf'
import samplePdf from './sample.pdf'
import { useEffect, useState } from 'react';

const csvData = [
  ["firstname", "lastname", "email"],
  ["Ahmed",      "Tomi",     "ah@smthing.co.com"],
  ["Raed",      "Labes",     "rl@smthing.co.com"],
  ["Yezzi",     "Min l3b", "ymin@cocococo.com"],
  ["Yezzi", "Min l3b", "ymin@cocococo.com"],
];


function AllPages(props) {
  const [numPages, setNumPages] = useState(null);

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }

  const { pdf } = props;

  return (
    <Document
      file={pdf}
      onLoadSuccess={onDocumentLoadSuccess}
    >
      {Array.from(new Array(numPages), (el, index) => (
        <Page key={`page_${index + 1}`} pageNumber={index + 1} />
      ))}
    </Document>
  );
}

function App() {
  const [numPages, setNumPages] = useState(null)
  useEffect(() => {
    pdfjs.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`
  })
  
  return (
    <div className="App">
      <h1>Media Handling</h1>
      {/* Media Handling */}
      <ReactPlayer 
        url="https://www.youtube.com/watch?v=Zy0y_gnyeJY"
        width="600px"
        controls={true}
        volume={0}
      />

      <CSVLink data={csvData} >Download Data CSV</CSVLink>

      
      <div className='all-page-container' >
        <AllPages pdf={samplePdf} />
      </div>
      
    

      {/* Audio Handling */}
      {/* <ReactPlayer 
      url="https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3"
      playing={true}
      controls={true}
      /> */}
    </div>
  );
}

export default App;
